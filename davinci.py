from datetime import timedelta
import glob
import json
import xmltodict

import utils


# Currently only works on vides with a single sequence. :shrug:
def parse_xml():
    """ Return the framerate and clips from the sequence"""

    files = glob.glob('*.xml')
    if len(files) > 1:
        for i, item in enumerate(files):
            print(i, item)
        selection = int(input('Which file do you want to process?\n'))
        xml_file = files[selection]
    else:
        xml_file = files[0]
    with open(xml_file, 'r') as f:
        xml_info = xmltodict.parse(f.read())
        #print(json.dumps(xml_info, indent=4)) 
        sequence = xml_info["xmeml"]["sequence"]
        framerate = sequence["rate"]["timebase"]
        if isinstance(sequence["media"]["video"]["track"], list):
            return int(framerate), sequence["media"]["video"]["track"][0]["clipitem"]
        else:
            return int(framerate), sequence["media"]["video"]["track"]["clipitem"]


def get_timestamp(frame, framerate):
    """ This calculates where each clip starts in the video."""
    # https://stackoverflow.com/a/28379008
    return timedelta(seconds=(int(frame) / framerate))


def get_description():
    clip_info = []
    framerate, clips = parse_xml()
    for clip in clips:
        timestamp = get_timestamp(clip['start'], framerate)
        # Remove the microseconds from the timestamp, youtube doesn't use them.
        timestamp = timestamp - timedelta(microseconds=timestamp.microseconds)

        if "DSC" not in clip['name'] and "video" not in clip['name'] and "mp3" not in clip['name'] and "GH" not in clip['name'] and "mov" not in clip['name']:
            clip_info.append((
                timestamp,
                clip['name'],
                int(clip['start']))
            )
    try:
        start, end = utils.dir_iso_date()
        YOUTUBE=f"""1985 mercury capri - Timelapse
        
    {start} to {end}
        
        """

        print(YOUTUBE)
    except:
        pass
    for desc in sorted(clip_info, key=lambda clip: clip[2]):
        print(f'{desc[0]} {desc[1]}')
