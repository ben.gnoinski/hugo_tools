import datetime
import glob
import os

CAPRI_VIDEOS='/data/capri_videos'
GALLERY='/mnt/capri/gallery_originals'
SD_CARD='/media/ben'

def dir_iso_date():
    current_dir = os.getcwd()
    current_dir = current_dir.split('/')[-1]
    current_dir = current_dir.split('_')
    year = current_dir[0]
    week = current_dir[2]
    first_date = datetime.datetime.strptime(f'{year} {week} 1', '%G %V %u').date().strftime('%b %d')
    last_date = datetime.datetime.strptime(f'{year} {week} 7', '%G %V %u').date().strftime('%b %d %Y')
    return first_date, last_date

def chdir(d):
    print(f'cd {d}')
    os.chdir(d)

def get_files(d):
    all_files = []
    for dirpath, dirname, filenames in os.walk(d):
        all_files += filenames
    return all_files


def missing_files():
    ''' Usefull for finding out if I have 
    forgottent to copy files from the sd card
    to the gallery, or capri_videos
    '''
    gf = get_files(GALLERY)
    cv = get_files(CAPRI_VIDEOS)
    sd = get_files(SD_CARD)

    # allows me to iterate through all files on the sd card
    # and compare against a single list
    combined = gf + cv

    missed_copy = []
    for filename in sd:
        if filename not in combined:
            missed_copy.append(filename)

    print(missed_copy)
missing_files()
