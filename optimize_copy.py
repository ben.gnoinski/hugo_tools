import glob
import os
import shutil
from PIL import Image
from config import DELETED_ITEMS

def copy_image(src, dest):
    shutil.copyfile(src, dest)


def get_file_list(folder):
    cwd = os.getcwd()
    os.chdir(folder)
    file_list = glob.glob("*")
    os.chdir(cwd)
    return file_list


def main():
    os.chdir(DELETED_ITEMS)
    deleted_files = glob.glob(f'*')
    
    os.chdir('/mnt/capri')
    gallery_folders = glob.glob("gallery_originals/*")

    for gallery_folder in gallery_folders:
    
        original_file_list = get_file_list(gallery_folder)
        static_files = []
        static_folder = f'static/gallery/{gallery_folder.split("/")[1]}'
    
    
        if os.path.exists(static_folder):
            static_files = get_file_list(static_folder)
        else:
            os.mkdir(static_folder)
    
        new_gallery_files = [i for i in original_file_list if i not in static_files and i not in deleted_files]

        new_width = 1920
    
        for image in new_gallery_files:
            gallery_file = f'{gallery_folder}/{image}'
            static_file = f'{static_folder}/{image}'
    
            #shutil.copyfile(gallery_file, static_file)
            im = Image.open(gallery_file)
            width, height = im.size
            ratio = (new_width / float(width))
    
            new_size = (new_width, int(height * ratio))
    
            im = im.resize(new_size)
            im.save(static_file)

if __name__ == "__main__":
  main()
