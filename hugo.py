#!/usr/bin/env python3
import glob
import os
import shutil

import click

from subprocess import call, check_output

from config import HUGO_BASE, DOCKER_BUILD, DOCKER_DEV, DOCKER_DEV_RUNNING, DOCKER_PUBLISH, SITE_UPLOAD


def clean_command():
    container = check_output(DOCKER_DEV_RUNNING).decode().rstrip("\n")
    if not container:
        print('There is no container currently')
        pass
    else:
        actions = ['kill', 'rm']
        for action in actions:
            command = ['docker', action, container]
            print('%s %s' % (action, container))
            call(command)
    # Not cleaning the public folder cause hugo copies static files every time
    # output_files = glob.glob(f'{HUGO_BASE}/public/*')
    # for file_to_remove in output_files:
    #     try:
    #         os.remove(file_to_remove)
    #     except IsADirectoryError:
    #         shutil.rmtree(file_to_remove)


def build_command():
    call(DOCKER_BUILD)


def dev_command():
    clean_command()
    build_command()
    print(' '.join(DOCKER_DEV))
    call(DOCKER_DEV)


def publish_command():
    clean_command()
    build_command()
    call(DOCKER_PUBLISH)


def upload_command():
    publish_command()
    call(SITE_UPLOAD)
    # call(['aws', 'cloudfront', 'create-invalidation', '--distribution-id', 'EW7T5A29H3R3J', '--paths', '/*'])


@click.group()
def cli():
    pass


@cli.command()
def clean():
    clean_command()


@cli.command()
def build():
    build_command()


@cli.command()
def dev():
    dev_command()


@cli.command()
def publish():
    publish_command()


@cli.command()
def upload():
    upload_command()
