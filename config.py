import os


HUGO_TOOLS_PATH = os.path.dirname(os.path.realpath(__file__))

print(HUGO_TOOLS_PATH)
# Folder where any deleted items will be moved
DELETED_ITEMS = '/mnt/capri/deleted'

# Used when running or stopping the hugo container
DOCKER_CONTAINER_NAME = 'capridev'

# Used when building the image and starting the container
DOCKER_IMAGE_NAME = 'capri'

# Folder where all of the hugo files live
HUGO_BASE = '/home/ben/capri'

# Command to execute in the docker container
HUGO_EXECUTABLE = '/usr/local/bin/hugo'

# Folder where the jinja templates live
JINJA_TEMPLATES = '/mnt/capri/jinja_templates'

# Command for building the docker container
DOCKER_BUILD = [
    'docker',
    'build',
    '-t',
    f'{DOCKER_CONTAINER_NAME}:latest',
    '-f',
    f'{HUGO_TOOLS_PATH}/docker/Dockerfile',
    '.'
]

# Command used to start the dev docker container
DOCKER_DEV = [
    'docker',
    'run',
    '-td',
    '-p',
    '1313:1313',
    '-v',
    f'{HUGO_BASE}:/hugo',
    '-v',
    '/mnt/capri/static:/static',
    '--name',
    f'{DOCKER_CONTAINER_NAME}',
    '-u',
    os.getenv('USER'),
    f'{DOCKER_IMAGE_NAME}:latest',
    f'{HUGO_EXECUTABLE}',
    'server',
    '-D',
    '--bind',
    '0.0.0.0',
    '--config',
    'config.toml',
]

# Command to check and see if the container is already running
DOCKER_DEV_RUNNING = [
    'docker',
    'ps',
    '-af',
    f'name={DOCKER_CONTAINER_NAME}',
    '-q'
]

# Command for publishing
DOCKER_PUBLISH = [
    'docker',
    'run',
    '-v',
    f'{HUGO_BASE}:/hugo',
    '-v',
    '/mnt/capri/static:/static',
    '--name',
    f'{DOCKER_CONTAINER_NAME}',
    '-u',
    os.getenv('USER'),
    f'{DOCKER_IMAGE_NAME}:latest',
    f'{HUGO_EXECUTABLE}',
    '--environment',
    'production',
]

# Command for uploading files to s3
SITE_UPLOAD = [
    'aws',
    '--profile',
    'jfic',
    's3',
    'sync',
    '--delete',
    '%s/public' % os.getcwd(),
    's3://theannihilator.ca'
]
