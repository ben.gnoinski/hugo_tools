from setuptools import setup
setup(
    name='Hugo',
    version='0.0.1',
    entry_points={
        'console_scripts': [
            'copy_gallery=optimize_copy:main',
            'gallery=gallery:gallery',
            'rename=gopro_video_rename:main',
            'post=gallery:post',
            'hugo=hugo:cli',
            'olive=olive_tools:get_description',
            'davinci=davinci:get_description'
        ]
    }
)
