Tools that I use for building my Hugo site

### gallery.py (gallery)
* Create gallery folder in hugo
* iterate over all images in a folder
* Open each image
* Capture a user input description of the picture
** if input is `delete` move picture to a deleted items folder.
* Build a hugo gallery list compatible with the [zzo Theme](https://themes.gohugo.io/hugo-theme-zzo/)
* use a jinja template to write out the gallery post

### gopro_video_rename.py (rename)
* Get all files starting with GH in the current folder
* Build a sorted listed of files based on the modified time of the files
* print a list of files to be renamed
* Ask for input
** If `y` rename the files

### hugo.py (hugo)
* Provides a set of commands for developing with hugo
* uses config.py to get paths and commands to run

## hugo commands

**build** - Builds the docker container
**clean** - Kills and removes dev container, removes any hugo published files
**dev** - runs `clean`, `build`, then starts up a hugo dev container localy listentin on port 1313
**publish** - runs a hugo container which publishes your files for upload
**upload** - upload your files to s3 for hosting
