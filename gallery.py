import datetime
import glob
import logging
import os
import shutil
import sys

from subprocess import Popen

from config import HUGO_BASE, DELETED_ITEMS, JINJA_TEMPLATES
from jinja2 import Environment, FileSystemLoader
from PIL import Image
from yaml import dump


if 'debug' in sys.argv or '-d' in sys.argv:
    loglevel = logging.DEBUG
else:
    loglevel = logging.INFO

date_time = datetime.datetime.now().strftime('%Y-%m-%dT%X%z')
jinja_env = Environment(
    loader=FileSystemLoader(JINJA_TEMPLATES)
)
images = {"images": []}
logging.basicConfig(format='%(message)s', level=loglevel)


def get_meta_data(post_type, current_dir):
    logging.info(f'Current working dir: {current_dir}')
    title = input(f'Please enter a {post_type} title?\n')
    title = f'{title} - {post_type}'
    description = input(f'Please enter a {post_type} description?\n')
    if post_type == 'Writeup':
        tags = input('enter comma seperated tags for this post.\n').split(',')
        return title, description, tags
    return title, description


def get_current_dir():
    return os.getcwd().split('/')[-1]


def kill_display_process():
    """ Kill display processes

   `display` is linux's imagemagic image viewer executable name
    this just very naively kills all display processes
    """
    Popen(['pkill', 'display'])


def make_gallery_dir(gallery_dir):
    logging.info(f'{gallery_dir}/index.md')
    if not os.path.exists(gallery_dir):
        logging.info(f'creating {gallery_dir}')
        os.mkdir(gallery_dir)
    elif os.path.isfile(f'{gallery_dir}/index.md'):
        proceed = input(f'{gallery_dir}/index.md already exists do you wish to continue?\n')
        if proceed != 'y':
            sys.exit(1)


def move_file(f):
    if not os.path.exists(DELETED_ITEMS):
        os.mkdir(DELETED_ITEMS)
    shutil.move(f, DELETED_ITEMS)


def gallery():
    current_dir = get_current_dir()
    gallery_dir = f'{HUGO_BASE}/content/gallery/{current_dir}'

    make_gallery_dir(gallery_dir)

    gallery_title, gallery_description = get_meta_data('Photo Gallery', current_dir)

    for image in sorted(glob.glob('*.JPG')):
        galimage = Image.open(image)
        galimage_resized = galimage.resize((1024, 768))
        galimage_resized.show()
        description = input('Description for the image?\n')
        if description == 'delete':
            move_file(image)
            kill_display_process()
            continue
        # PIL.Image.show() does not close the photo after it's opened
        kill_display_process()

        images["images"].append({'image': image, "caption": description})

    gallery_template = jinja_env.get_template('gallery.md.j2')

    rendered_gallery = gallery_template.render(
        gallery_title=gallery_title,
        date=date_time,
        description=gallery_description,
        images=dump(images)
    )
    logging.debug(rendered_gallery)

    with open(f'{gallery_dir}/index.md', 'w') as f:
        f.write(rendered_gallery)


def post():
    post_dir = f'{HUGO_BASE}/content/posts'

    title, description, tags = get_meta_data('Writeup', get_current_dir())
    post_template = jinja_env.get_template('post.md.j2')

    rendered_post = post_template.render(
        date=date_time,
        description=description,
        tags=tags,
        title=title
    )
    logging.info(rendered_post)
    with open(f'{post_dir}/{title.replace(" ", "_")}.md', 'w') as f:
        f.write(rendered_post)
