import glob
import os


def last_video_file():
    """ If no video_xxx.MP4 files exist return 0

    otherwise find the highest video_xxx.MP4 value and return it
    ex: 23
    """

    video_files = glob.glob('video_*.MP4')
    if not video_files:
        return 0
    else:
        last_video = sorted(video_files)[-1].split('_')[1].split('.')[0]
        return int(last_video) + 1


def rename_file(original, new):
    os.rename(original, new)


def sort_gopro_files(gopro_files):
    # get all of the 4 digit files
    four_digits = {}
    for gopro in gopro_files:
        video = gopro[4:8]
        chapter = gopro[2:4]
        four_digits.setdefault(video, [])
        four_digits[video].append(chapter)

    sorted_videos = sorted((four_digits))
    ordered_files = []
    for video in sorted_videos:
        chapters = sorted(four_digits[video])
        for chapter in chapters:
            ordered_files.append(f'GH{chapter}{video}.MP4')

    return ordered_files


def main():
    """Rename gopro videos from GH*.MP4 to video_xxx.MP4"""

    gopro_files = glob.glob('GH*.MP4')
    if gopro_files:
        new_file_names = []
        sorted_files = sort_gopro_files(gopro_files)
        last_video = last_video_file()

        for value in range(last_video, last_video + len(sorted_files)):
            new_file_names.append(f'video_{str(value).zfill(3)}.MP4')

        rename_files = tuple(zip(sorted_files, new_file_names))

        for original_file, new_file in rename_files:
            print(original_file, f'-> {new_file}')

        if input('Would you like to rename the above files?\n') == 'y':
            with open('original_files.txt', 'a+') as f:
                for original_file, new_file in rename_files:
                    f.write(f'{original_file} to {new_file}\n')
                    print(f'renaming {original_file} to {new_file}')
                    rename_file(original_file, new_file)


if __name__ == "__main__":
    main()
