from datetime import timedelta
import glob

import xmltodict


# Currently only works on vides with a single sequence. :shrug:
def parse_ove():
    """ Return the framerate and clips from the sequence"""

    olive_files = glob.glob('*.ove')
    if len(olive_files) > 1:
        for i, item in enumerate(olive_files):
            print(i, item)
        selection = int(input('Which file do you want to process?\n'))
        olive_file = olive_files[selection]
    else:
        olive_file = olive_files[0]
    with open(olive_file, 'r') as f:
        xml_info = xmltodict.parse(f.read())
        sequence = xml_info["project"]["sequences"]["sequence"]
        framerate = sequence['@framerate'].split('.')[0]
        return int(framerate), sequence['clip']


def get_timestamp(frame, framerate):
    """ This calculates where each clip starts in the video."""
    # https://stackoverflow.com/a/28379008
    return timedelta(seconds=(int(frame) / framerate))


def get_description():
    clip_info = []
    framerate, clips = parse_ove()
    for clip in clips:
        timestamp = get_timestamp(clip['@in'], framerate)
        # Remove the microseconds from the timestamp, youtube doesn't use them.
        timestamp = timestamp - timedelta(microseconds=timestamp.microseconds)

        if clip['@stream'] == "0" and "DSC" not in clip['@name'] and "video" not in clip['@name'] and "mp3" not in clip['@name']:
            clip_info.append((
                timestamp,
                clip['@name'],
                int(clip['@in']))
            )
    for desc in sorted(clip_info, key=lambda clip: clip[2]):
        print(f'{desc[0]} {desc[1]}')
